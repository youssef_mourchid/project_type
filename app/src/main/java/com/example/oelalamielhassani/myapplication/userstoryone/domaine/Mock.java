package com.example.oelalamielhassani.myapplication.userstoryone.domaine;

import com.example.oelalamielhassani.myapplication.userstoryone.domaine.interfaces.IMock;
import com.example.oelalamielhassani.myapplication.userstoryone.utils.AppliConfig;

/**
 * Created by oelalamielhassani on 23/02/2018.
 */

public class Mock implements IMock {
    public Mock() {
    }

    @Override
    public String getHello(int type) {

        String message = "";
        switch (type) {
            case 1:
                message = AppliConfig.HELLO_FROM_CONSTANTS_ONE;
                break;
            case 2:
                message = AppliConfig.HELLO_FROM_CONSTANTS_TWO;
                break;
            default:
                message = AppliConfig.HELLO_FROM_CONSTANTS_DEFAULT;
        }
        return message;
    }
}
