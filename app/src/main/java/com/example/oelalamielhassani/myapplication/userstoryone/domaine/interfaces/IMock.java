package com.example.oelalamielhassani.myapplication.userstoryone.domaine.interfaces;

/**
 * Created by oelalamielhassani on 23/02/2018.
 */

public interface IMock {
    String getHello(int type);
}
